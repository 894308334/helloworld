package com.example.springboot_profile.config;

import com.example.springboot_profile.bean.Sheep;
import com.example.springboot_profile.bean.User;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
@EnableConfigurationProperties(Sheep.class)//导入第三方写好的组件进行属性绑定
//SpringBoot默认只扫描自己主程序所在的包，如果导入第三方包，即使组件标注上了@Component,@ConfigurationProperties注解也没用，因为扫描不到
@SpringBootConfiguration//这是一个配置类，替代以前的配置文件
public class AppConfig {
    @Bean("haha")
    public User user(){
        User user = new User();
        user.setId(1L);
        user.setName("陈清焰");
        return user;
    }

}

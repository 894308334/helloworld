package com.example.springboot_profile.config;

import com.example.springboot_profile.bean.Cat;
import com.example.springboot_profile.bean.Dog;
import com.example.springboot_profile.bean.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.annotation.Bean;

@SpringBootConfiguration//这是一个配置类，替代以前的配置文件
public class AppConfig2 {
    @ConditionalOnClass(name = "com.alibaba.druid.FastsqlException")
    @Bean
    public Cat cat01(){
        return new Cat();
    }
    @ConditionalOnMissingClass(value = "com.alibaba.druid.FastsqlException")
    @Bean
    public Dog dog01(){
        return new Dog();
    }
    @ConditionalOnBean(value= Dog.class)
    @Bean
    public  User zhangsan(){
        return new User();
    }
    @ConditionalOnMissingBean(value = Dog.class)
    @Bean
    public  User lisi(){
        return new User();
    }



}

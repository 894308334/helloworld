package com.controller;


import com.config.Enterprise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
public class BookController {
    @Value("${enterprise.name}")
    private String name;
    @Value("${enterprise.age}")
    private Integer age;
    @Value("${enterprise.subject[1]}")
    private String subject;
    @Autowired
    private Environment environment;
    @Autowired
    private Enterprise enterprise;


    @GetMapping ("/save")
    public String save(){
        //使用@Value注入
        System.out.println("name:"+name);
        System.out.println("age:"+age);
        System.out.println("subject:"+subject);
        System.out.println("---------------");
        //注入environment
        System.out.println("name:"+environment.getProperty("enterprise.name"));
        System.out.println("age:"+environment.getProperty("enterprise.age"));
        System.out.println("subject:"+environment.getProperty("enterprise.subject[2]"));
        System.out.println("-----------------");
        System.out.println(enterprise);
        return "hello,springboot";
    }
}

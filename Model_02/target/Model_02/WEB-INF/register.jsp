<%--
  Created by IntelliJ IDEA.
  User: Ciao
  Date: 2024/3/25
  Time: 16:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>注册</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/WEB-INF/register" method="post" name="registerFrom">
    <table>
        <tr>
            <td>姓名：</td>
            <td><input type="text" name="uname" value="${user.name}"></td>
        </tr>
        <tr>
            <td>密码：</td>
            <td><input type="password" name="upass"></td>
        </tr>
        <tr>
            <td>确认密码：</td>
            <td><input type="password" name="reupass"></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="submit" value="注册">
            </td>
        </tr>
    </table>
</form>
</body>
</html>

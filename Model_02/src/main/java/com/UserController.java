package com;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.pojo.UserFrom;


import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/WEB-INF")
public class UserController {
    @PostMapping("/login")
    public  String login(UserFrom user, HttpSession session, Model model){
        if ("zhangsan".equals(user.getUname())&&"123456".equals(user.getUpass())){
                session.setAttribute("u",user);
                return "/main";//登陆成功，跳转到main.jsp
        }else{
            model.addAttribute("messageError","用户名或密码错误");
            return "/login";
        }
    }

    @PostMapping("/register")
    public  String register(UserFrom user, Model model){
        if ("zhangsan".equals(user.getUname())&&"123456".equals(user.getUpass())){
            return "/login";//登陆成功，跳转到login.jsp
        }else{
            model.addAttribute("uname",user.getUname());
            return "/register";//返回register.jsp
        }
    }
}

<%--
  Created by IntelliJ IDEA.
  User: Ciao
  Date: 2024/3/25
  Time: 16:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>登录</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/WEB-INF/login" method="post">
    <table>
        <tr>
            <td>姓名：</td>
            <td><input type="text" name="uname" ></td>
        </tr>
        <tr>
            <td>密码：</td>
            <td><input type="password" name="upass"></td>
        </tr>
        <tr>
            <td colspan="2" >
                <input type="submit" value="提交">
                <input type="reset" value="重置">
            </td>
        </tr>
    </table>
    <h1>${messageError}</h1>
</form>
</body>
</html>
